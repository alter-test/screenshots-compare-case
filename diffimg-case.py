import pathlib

from diffimg import diff

res_dir = pathlib.Path(__file__).parent / 'resources'

scr1 = (res_dir / 'scr1.png').resolve()
scr2 = (res_dir / 'scr2.png').resolve()

percent = diff(str(scr1), str(scr2),ignore_alpha=True)

print(percent)