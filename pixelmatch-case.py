from PIL import Image

from pixelmatch.contrib.PIL import pixelmatch

img_a = Image.open("resources/scr1.png")
img_b = Image.open("resources/scr2.png")
img_diff = Image.new("RGBA", img_a.size)

# note how there is no need to specify dimensions
mismatch = pixelmatch(img_a, img_b, img_diff, includeAA=True)

# diff-percent
percent = mismatch / (img_a.width * img_a.height)
img_diff.save("pixelmatch_diff.png")

# assert if screen difference less than 0.5 percent
assert percent < 0.5


pass